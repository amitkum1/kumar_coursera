/******************************************************************************
 * Copyright (C) 2017 by Alex Fosdick - University of Colorado
 *
 * Redistribution, modification or use of this software in source or binary
 * forms is permitted as long as the files maintain this copyright. Users are 
 * permitted to modify this and use it to learn about the field of embedded
 * software. Alex Fosdick and the University of Colorado are not liable for any
 * misuse of this material. 
 *
 *****************************************************************************/
/**
 * @file stats.h
 * @brief Header file for stat.c  
 *
 * Here we declare all different functions that are used by stats.c
 * 
 *
 * @author Amit Kumar<Add FirsName LastName>
 * @date 08/09/2017 
 *
 */

#ifndef __STATS_H__
#define __STATS_H__

/* Add Your Declarations and Function Comments here */ 

/**
 * @brief This function sorts the input array
 *
 *
 * @param inArray Input Array 
 * @param inLenght Size of the array
 *
 * @return void This function doesn't return anything
 */

void sort_array( unsigned char * inArray, unsigned int inLength ) {
  for (unsigned int i = 0; i < inLength - 1 ; i++ ) {
    for (unsigned int j = i + 1; j < inLength ; j++ ) {
      if ( inArray[j] > inArray[i] ) {
	unsigned char temp = inArray[i];
	inArray[i] = inArray[j] ;
	inArray[j] = temp;
      }
    }
  }
}

/**
 * @brief This function finds the median of the array
 * 
 * @param inArray Input Array 
 * @param inLenght Size of the array
 *
 * @return Returns calculated median 
 */

unsigned char find_median(unsigned char * inArray, unsigned int inLength ) {
  // Here array is sorted so that it function can be used on unsorted
  // array too.
  sort_array(inArray, inLength);
  unsigned int middleIdx = (inLength / 2);
  if (inLength % 2 == 0 ) {
    return ((inArray[(middleIdx - 1)] + inArray[middleIdx])/ 2);
  }
  else {
    return inArray[middleIdx];
  } 
}

/**
 * @brief This function finds the median of the array
 * 
 * @param inArray Input Array 
 * @param inLenght Size of the array
 *
 * @return Returns calculated mean 
 */
unsigned char find_mean(unsigned char * inArray, unsigned int inLength ) {
  unsigned int sum = 0;
  unsigned char mean ; 
  for (unsigned int i = 0; i < inLength ; i++ ) {
    sum = sum + inArray[i];
  }
  mean = sum / inLength;
  return mean;
}


/**
 * @brief This function finds the median of the array
 * 
 * @param inArray Input Array 
 * @param inLenght Size of the array
 *
 * @return Returns maximum of the array items.
 */

unsigned char find_max(unsigned char * inArray, unsigned int inLength ) {
  sort_array(inArray, inLength);
  return inArray[0];
}

/**
 * @brief This function finds the median of the array
 * 
 * @param inArray Input Array 
 * @param inLenght Size of the array
 *
 * @return Returns minimum of the array items.
 */

unsigned char find_min(unsigned char * inArray, unsigned int inLength ) {
  sort_array(inArray, inLength);
  return inArray[inLength - 1];
}


/**
 * @brief This function prints statistics of input array
 *
 * Function is used to print Median, Mean, Max and Min of the 
 * array
 *
 * @param inArray Input Array 
 * @param inLenght Size of the array
 *
 * @return void This function doesn't return anything
 */
void print_statistics( unsigned char * inArray, unsigned int inLength ) {
  unsigned char lmedian = find_median(inArray, inLength);
  unsigned char lmean   = find_mean(inArray, inLength);
  unsigned char lmax    = find_max(inArray, inLength);
  unsigned char lmin    = find_min(inArray, inLength);
  printf(" \n Median: %d Mean: %d Maximum: %d Minimum: %d", lmedian, lmean, lmax, lmin);
}

/**
 * @brief This function prints array
 *
 *
 * @param inArray Input Array 
 * @param inLenght Size of the array
 *
 * @return void This function doesn't return anything
 */
void print_array( unsigned char * inArray, unsigned int inLength ) {
  unsigned int i = 0;
  for ( i=0 ; i< inLength; i++ ) {
    printf( " %d ", *inArray++);
  }
}

#endif /* __STATS_H__ */
